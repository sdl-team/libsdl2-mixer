Source: libsdl2-mixer
Section: libs
Priority: optional
Maintainer: Debian SDL packages maintainers <pkg-sdl-maintainers@lists.alioth.debian.org>
Uploaders: Manuel A. Fernandez Montecelo <mafm@debian.org>, Simon McVittie <smcv@debian.org>
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               libflac-dev,
               libfluidsynth-dev,
               libmpg123-dev,
               libogg-dev,
               libopusfile-dev,
               libsdl2-dev,
               libtool,
               libvorbis-dev,
               libxmp-dev,
               pkgconf,
Homepage: https://github.com/libsdl-org/SDL_mixer
Vcs-Browser: https://salsa.debian.org/sdl-team/libsdl2-mixer
Vcs-Git: https://salsa.debian.org/sdl-team/libsdl2-mixer.git


Package: libsdl2-mixer-2.0-0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${dlopen:Recommends},
            timgm6mb-soundfont | sf3-soundfont-gm
Description: Mixer library for Simple DirectMedia Layer 2, libraries
 SDL_mixer is a sample multi-channel audio mixer library.  It supports any
 number of simultaneously playing channels of 16 bit stereo audio, plus a single
 channel of music, mixed by the popular FLAC, libxmp MOD, FluidSynth and
 Timidity MIDI, Ogg Vorbis, and mpg123 MP3 libraries.
 .
 This package contains the shared library.

Package: libsdl2-mixer-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libflac-dev,
         libmpg123-dev,
         libogg-dev,
         libopusfile-dev,
         libsdl2-dev,
         libsdl2-mixer-2.0-0 (= ${binary:Version}),
         libvorbis-dev,
         libxmp-dev,
Description: Mixer library for Simple DirectMedia Layer 2, development files
 SDL_mixer is a sample multi-channel audio mixer library.  It supports any
 number of simultaneously playing channels of 16 bit stereo audio, plus a single
 channel of music, mixed by the popular FLAC, libxmp MOD, FluidSynth and
 Timidity MIDI, Ogg Vorbis, and mpg123 MP3 libraries.
 .
 This package contains the development files.
